//
//  DataManager.swift
//  ContactListHW
//
//  Created by Maksim on 27.10.2023.
//

final class DataManager {
    
    static let shared = DataManager()
    
    private init() {}
    
    let names = [
        "Вася", "Петя", "Тимон", "Коля",
        "Толя", "Прохор", "Ашот",
        "Вова", "Евген", "Армэн"
    ]
    
    let surnames = [
        "Алиев", "Иванов", "Смирнов", "Захаренко",
        "Мирошниченко", "Моисеев", "Джигарханян",
        "Бронников", "Мирославский", "Сидоров"
    ]
    
    let emails = [
        "telepuzik23@mail.ru", "lolovi4@mail.ru", "zagogulya@mail.ru",
        "ktozheya@mail.ru", "zigmund_tree@mail.ru", "nagibator777@mail.ru",
        "zloy-cat@mail.ru", "zumbadance@mail.ru", "mypochta@mail.ru",
        "nospam11@mail.ru"
    ]
    
    let phones = [
        "+79801258870", "+79622135412", "+79971222015", "+79082035998",
        "+79992341371", "+79885561325", "+79069854613", "+79095689111",
        "+79120231173", "+79162167162"
    ]
}
