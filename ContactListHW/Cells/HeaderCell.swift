//
//  HeaderCell.swift
//  ContactListHW
//
//  Created by Maksim on 27.10.2023.
//

import UIKit

final class HeaderCell: UITableViewCell {
    @IBOutlet var personLabel: UILabel!
}
