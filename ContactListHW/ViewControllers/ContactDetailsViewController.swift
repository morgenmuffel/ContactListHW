//
//  ContactDetailsViewController.swift
//  ContactListHW
//
//  Created by Maksim on 27.10.2023.
//

import UIKit

final class ContactDetailsViewController: UIViewController {
    
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = person.fullName
        emailLabel.text = "Почта: \(person.email)"
        phoneNumberLabel.text = "Телефон: \(person.phoneNumber)"
    }
}
