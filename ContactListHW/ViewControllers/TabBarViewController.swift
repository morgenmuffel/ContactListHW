//
//  TabBarViewController.swift
//  ContactListHW
//
//  Created by Maksim on 27.10.2023.
//

import UIKit

final class TabBarViewController: UITabBarController {
    
    let persons = Person.getContactList()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewControllers(with: persons)
    }
    
    private func setupViewControllers(with persons: [Person]) {
        guard let contactListVC = viewControllers?.first as? ContactListViewController else { return }
        guard let sectionVC = viewControllers?.last as? SectionTableViewController else { return }
        
        contactListVC.persons = persons
        sectionVC.persons = persons
    }
}
